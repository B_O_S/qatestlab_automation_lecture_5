package QATestLAb_automation_Lecture_5;


import QATestLAb_automation_Lecture_5.model.ProductData;
import QATestLAb_automation_Lecture_5.utils.Properties;
import QATestLAb_automation_Lecture_5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.Random;

import static QATestLAb_automation_Lecture_5.utils.DataConverter.*;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private ProductData randProductData;
    private Random rand = new Random();
    private String URLrandProductPage;
    private Actions builder;


    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void CheckSiteVersion(String browser) {
        // TODO implement logic to check correct version opened page
        driver.get(Properties.getBaseUrl());
        try {
            Assert.assertEquals(driver.findElement(By.className("carousel-inner")).isEnabled(), !BaseTest.isMobileTesting(browser));
            System.out.println("\033[32;1mChecking the site version correct - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the site version correct - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the site version correct - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the site version correct - failed");
            System.out.println(assErrE);
        } catch (Exception e) {
            if (BaseTest.isMobileTesting(browser)) {
                System.out.println("\033[32;1mChecking the site version correct - \033[36;1mpassed\033[0m");
                CustomReporter.logAction("Checking the site version correct - passed");
            } else {
                System.out.println("\033[32;1mChecking the site version correct - \033[31;1mfailed\033[0m");
                CustomReporter.logAction("Checking the site version correct - failed");
            }
        }

        if (browser.equals("mobile")) ((JavascriptExecutor)driver).executeScript(
            "arguments[0].scrollIntoView();",
                    driver.findElement(By.xpath("//section[@id='content']/section/a")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@id='content']/section/a")));
        driver.findElement(By.xpath("//section[@id='content']/section/a")).click();
        waitForContentLoad();

        throw new UnsupportedOperationException();
    }

    public void OpenRandomProduct(String browser) {
        // TODO implement logic to open random product before purchase
        /**переходим по ссылке "все товары"*/

        List<WebElement> products = driver.findElements(By.xpath("//div[@id='js-product-list']" +
                                    "//article[@class='product-miniature js-product-miniature']"));
        int randProduct = rand.nextInt(products.size());
        if (randProduct == 0) randProduct = 1;
        WebElement we = driver.findElement(By.xpath("//div[@id='js-product-list']" +
                "//article[@class='product-miniature js-product-miniature'][" +
                randProduct + "]//div[@class='product-description']//a"));
        if (browser.equals("mobile")) ((JavascriptExecutor)driver).executeScript(
                "arguments[0].scrollIntoView();", we);
        wait.until(ExpectedConditions.visibilityOf(we));
        wait.until(ExpectedConditions.elementToBeClickable(we));
        we.click();

        URLrandProductPage = driver.getCurrentUrl();

        throw new UnsupportedOperationException();
    }

    public void SaveProductParameters() {

        CustomReporter.logAction("Get&save information about currently opened product");

        String nameRandProductData;
        nameRandProductData = driver.findElement(By
                .xpath("//section[@id='main']//div[@class='col-md-6']/h1")).getText();
        Integer qtyRandProductData;
        qtyRandProductData = parseStockValue(driver.findElement(By
                .cssSelector("#product-details > div.product-quantities > span")).getAttribute("textContent"));
        Float priceRandProductData;
        priceRandProductData = parsePriceValue(driver.findElement(By
                .xpath("//section[@id='main']//div[@class='current-price']/span[1]")).getText());
        randProductData = new ProductData(nameRandProductData, qtyRandProductData, priceRandProductData);

        throw new UnsupportedOperationException();
    }

    public void CartAddValidate() {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//form[@id='add-to-cart-or-refresh']//button[@class='btn btn-primary add-to-cart']")));
        driver.findElement(By.xpath(
        "//form[@id='add-to-cart-or-refresh']//button[@class='btn btn-primary add-to-cart']")).click();
        waitForContentLoad();

        try {
            Assert.assertEquals(driver.findElement(By.xpath("//h4[@id='myModalLabel']"))
                    .getAttribute("textContent"), "\uE876Товар успішно додано до Вашого кошика");
            System.out.println("\033[32;1mChecking the success adding product in the cart - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the success adding product in the cart - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the success adding product in the cart - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the success adding product in the cart - failed");
            System.out.println(assErrE);
        }

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//div[@id='blockcart-modal']//a[@class='btn btn-primary']")));
        driver.findElement(By.xpath(
                "//div[@id='blockcart-modal']//a[@class='btn btn-primary']")).click();
        waitForContentLoad();

        try {
            Assert.assertEquals(driver.findElements(By.xpath(
                 "//section[@id='main']//li[@class='cart-item']"))
                    .size(), 1);
            System.out.println("\033[32;1mChecking the quantity positions of product \"= 1\" in the cart - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the quantity positions of product '= 1' in the cart - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the quantity positions of product \"= 1\" in the cart - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the quantity positions of product '= 1' in the cart - failed");
            System.out.println(assErrE);
        }

        try {
            Assert.assertEquals(driver.findElement(By.xpath(
                    "//section[@id='main']//li[@class='cart-item']//a[@class='label']"))
                    .getAttribute("textContent").toUpperCase(), randProductData.getName());
            System.out.println("\033[32;1mChecking the correct name of product in the cart - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the correct name of product in the cart - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the correct name of product in the cart - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the correct name of product in the cart - failed");
            System.out.println(assErrE);
        }

        try {
            Assert.assertEquals(driver.findElement(By.xpath(
                    "//section[@id='main']//li[@class='cart-item'][1]" +
                            "//div[@class='product-line-info'][2]/span[@class='value']"))
                        .getAttribute("textContent").replaceAll("[^0-9?!\\,]",""),
                            randProductData.getPrice());
            System.out.println("\033[32;1mChecking the correct price of product in the cart - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the correct price of product in the cart - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the correct price of product in the cart - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the correct price of product in the cart - failed");
            System.out.println(assErrE);
        }

        throw new UnsupportedOperationException();
    }

    public void OrderingProduct() {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                "//section[@id='main']//a[@class='btn btn-primary']")));
        driver.findElement(By.xpath("//section[@id='main']//a[@class='btn btn-primary']")).click();
        waitForContentLoad();
        driver.findElement(By.xpath(
                "//form[@id='customer-form']//div[@class='form-group row ']" +
                        "[2]//input[@class='form-control']")).sendKeys("Oleg");
        driver.findElement(By.xpath(
                "//form[@id='customer-form']//div[@class='form-group row ']" +
                        "[3]//input[@class='form-control']")).sendKeys("Bura");
        driver.findElement(By.xpath(
                "//form[@id='customer-form']//div[@class='form-group row ']" +
                        "[4]//input[@class='form-control']")).
                sendKeys("B_O_S-" + rand.nextInt(999) + "@ukr.net");
        driver.findElement(By.xpath("//form[@id='customer-form']/footer/button")).click();
        waitForContentLoad();
        driver.findElement(By.xpath(
                "//div[@id='delivery-address']//div[@class='form-group row '][5]//input"))
                .sendKeys("Vyshneva str. 18/665");
        driver.findElement(By.xpath(
                "//div[@id='delivery-address']//div[@class='form-group row '][7]//input"))
                .sendKeys("33028");
        driver.findElement(By.xpath(
                "//div[@id='delivery-address']//div[@class='form-group row '][8]//input"))
                .sendKeys("Rivne");
        driver.findElement(By.xpath("//div[@id='delivery-address']//footer/button")).click();
        waitForContentLoad();
        driver.findElement(By.xpath("//section[@id='checkout-delivery-step']//button")).click();
        waitForContentLoad();
        driver.findElement(By.xpath("//input[@id='payment-option-2']")).click();
        driver.findElement(By.xpath("//input[@id='conditions_to_approve[terms-and-conditions]']")).click();
        driver.findElement(By.xpath("//div[@id='payment-confirmation']//button")).click();
        waitForContentLoad();

        throw new UnsupportedOperationException();
    }

    public void OrderSummaryValidate() {

        try {
            Assert.assertEquals(driver.findElement(By.xpath("//section[@id='content-hook_order_confirmation']//h3"))
                .getAttribute("textContent").trim(),
                    "\uE876Ваше замовлення підтверджено");
            System.out.println("\033[32;1mChecking the order is approved - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the order is approved - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the order is approved - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the order is approved - failed");
            System.out.println(assErrE);
        }

        try {
            Assert.assertEquals(driver.findElements(By.xpath(
                "//section[@id='content']//div[@class='order-line row']")).size(), 1);
            System.out.println("\033[32;1mChecking the quantity ordered positions of product \"= 1\" - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the quantity ordered positions of product '= 1' - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the quantity ordered positions of product \"= 1\" - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the quantity ordered positions of product '= 1' - failed");
            System.out.println(assErrE);
        }

        try {
            Assert.assertEquals(driver.findElement(By.xpath(
                "//section[@id='content']//div[@class='col-sm-4 col-xs-9 details']/span"))
                    .getAttribute("textContent").toUpperCase()
                        .substring(0, randProductData.getName().length()), randProductData.getName());
            System.out.println("\033[32;1mChecking the correct name of ordered product - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the correct name of ordered product - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the correct name of ordered product - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the correct name of ordered product - failed");
            System.out.println(assErrE);
        }

        try {
            Assert.assertEquals(driver.findElement(By.xpath(
                "//div[@id='order-items']//tbody/tr[3]/td[2]"))
                    .getAttribute("textContent").replaceAll("[^0-9?!\\,]",""),
                        randProductData.getPrice());
            System.out.println("\033[32;1mChecking the correct price of ordered product - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the correct price of ordered product - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the correct price of ordered product - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the correct price of ordered product - failed");
            System.out.println(assErrE);
        }

        driver.navigate().to(URLrandProductPage);
        waitForContentLoad();

        throw new UnsupportedOperationException();
    }

    public void CheckUpdatedInStock() {

        try {

            int a = Integer.parseInt(driver.findElement(By.cssSelector(
                    "#product-details > div.product-quantities > span")).
                    getAttribute("textContent").replaceAll("\\D+", "")) + 1;
            int b = randProductData.getQty();
            Assert.assertEquals(a, b);
            System.out.println("\033[32;1mChecking quantity products in stock was updated - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking quantity products in stock was updated - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking quantity products in stock was updated - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking quantity products in stock was updated - failed");
            System.out.println(assErrE);
        }

        throw new UnsupportedOperationException();
    }

/**Waits until page loader disappears from the page*/
    public void waitForContentLoad() {
        // TODO implement generic method to wait until page content is loaded
        wait.until(driver1 -> ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete"));
    }
}
