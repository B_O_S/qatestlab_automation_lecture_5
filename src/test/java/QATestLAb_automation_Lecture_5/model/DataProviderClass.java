package QATestLAb_automation_Lecture_5.model;

import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name = "data-provider_logindata")
    public static Object[][] dataProviderMethod() {
        return new Object[][]{{"webinar.test@gmail.com","Xcg7299bnSmMuRLp9ITw"}};
    }
}
