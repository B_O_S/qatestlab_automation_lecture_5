package QATestLAb_automation_Lecture_5;

import QATestLAb_automation_Lecture_5.utils.DriverFactory;
import QATestLAb_automation_Lecture_5.utils.logging.EventHandler;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

/**Base script functionality, can be used for all Selenium scripts.*/
public abstract class BaseTest extends DriverFactory {
    protected EventFiringWebDriver driver;
    protected GeneralActions actions;
    protected boolean isMobileTesting;

    /**Prepares {@link WebDriver} instance with timeout and browser window configurations.
     * Driver type is based on passed parameters to the automation project,
     * creates {@link ChromeDriver} instance by default.*/
    @Parameters({"selenium.browser", "selenium.grid"})
    @BeforeClass
    public void setUp(@Optional("mobile") String browser, @Optional("") String gridUrl) {
        // TODO create WebDriver instance according to passed parameters
          if (browser.equals("remote-chrome")) {
            driver = new EventFiringWebDriver(initDriver(browser, gridUrl));
        } else {
            driver = new EventFiringWebDriver(initDriver(browser));
        }
        driver.register(new EventHandler());
        isMobileTesting = isMobileTesting(browser);
        actions = new GeneralActions(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        // unable to maximize window in mobile mode
        if (!isMobileTesting) {
            driver.manage().window().setSize(new Dimension(1100,850));
            driver.manage().window().setPosition(new Point(0,0));
        }
        if (isMobileTesting) {
            driver.manage().window().setSize(new Dimension(250,850));
            driver.manage().window().setPosition(new Point(0,0));
        }

    }

    /**
     * Closes driver instance after test class execution.
     */
    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void ThroughTesting(String browser) {
        switch (browser) {
            case "firefox":
                System.out.println("\033[32;1mRunning tests through - \033[95;1mFireFox\033[0m");
                return;
            case "ie":
            case "internet explorer":
                System.out.println("\033[32;1mRunning tests through - \033[95;1mInternet Explorer\033[0m");
                return;
            case "headless-chrome":
                System.out.println("\033[32;1mRunning tests through - \033[95;1mHeadless Chrome\033[0m");
                return;
            case "mobile":
                System.out.println("\033[32;1mRunning tests through - \033[95;1mMobile Chrome\033[0m");
                return;
            case "remote-chrome":
                System.out.println("\033[32;1mRunning tests through - \033[95;1mRemote Chrome(Grid hub)\033[0m");
                return;
            case "chrome":
            default:
                System.out.println("\033[32;1mRunning tests through - \033[95;1mChrome\033[0m");
                return;
        }
    }


    /**
     *
     * @return Whether required browser displays content in mobile mode.
     */
    public static boolean isMobileTesting(String browser) {
        switch (browser) {
            case "mobile":
                return true;
            case "firefox":
            case "ie":
            case "internet explorer":
            case "chrome":
            default:
                return false;
        }
    }

    public static void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
