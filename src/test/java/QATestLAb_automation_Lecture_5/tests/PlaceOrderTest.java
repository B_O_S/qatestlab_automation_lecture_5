package QATestLAb_automation_Lecture_5.tests;

import QATestLAb_automation_Lecture_5.BaseTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class PlaceOrderTest extends BaseTest {

    // check site version

    @Parameters({"selenium.browser"})
    @Test
    public void CheckSiteVersOpenRandProdSaveProdParam(@Optional("mobile") String browser) {
        // TODO open main page and validate website version

        ThroughTesting(browser);

        try {
            actions.CheckSiteVersion(browser);
        } catch (UnsupportedOperationException e) {}

    // open random product

        try {
            actions.OpenRandomProduct(browser);
        } catch (UnsupportedOperationException e) {}

    // save product parameters

        try {
            actions.SaveProductParameters();
        } catch (UnsupportedOperationException e) {}
    }

    // add product to Cart and validate product information in the Cart

    @Test(dependsOnMethods={"CheckSiteVersOpenRandProdSaveProdParam"})
    public void CartAddValid() {
        try {
            actions.CartAddValidate();
        } catch (UnsupportedOperationException e) {}
    }

    // proceed to order creation, fill required information

    @Test(dependsOnMethods={"CartAddValid"})
    public void OrderingProd() {
        try {
            actions.OrderingProduct();
        } catch (UnsupportedOperationException e) {}
    }

    // place new order and validate order summary

    @Test(dependsOnMethods={"OrderingProd"})
    public void OrderSummValid() {
        try {
            actions.OrderSummaryValidate();
        } catch (UnsupportedOperationException e) {}
    }

    // checking quantity products in stock was updated

    @Test(dependsOnMethods={"OrderSummValid"})
    public void CheckUpdInStock() {
        try {
            actions.CheckUpdatedInStock();
        } catch (UnsupportedOperationException e) {}
    }
}
